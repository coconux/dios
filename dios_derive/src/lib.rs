
extern crate proc_macro;

use proc_macro::TokenStream;
use quote::quote;
use syn;
use regex::Regex;

#[proc_macro_derive(SerializableType)]
pub fn serializable_type_derive(input: TokenStream) -> TokenStream {
    // Construct a representation of Rust code as a syntax tree that we can manipulate
    let ast = syn::parse(input).unwrap();

    // Build the trait implementation
    impl_serializable_type(&ast)
}



fn impl_serializable_type(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let re = Regex::new("Message").unwrap();
    let string_name = name.to_string();
    let type_name = re.replace(string_name.as_str(), "");


    let generated = quote! {
        impl SerializableType for #name {
            fn get_type() -> String { String::from(#type_name) }
        }
    };

    generated.into()
}

