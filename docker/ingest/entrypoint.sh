#!/bin/sh

# shellcheck disable=SC2016
envs='
${APP_HOST}
${APP_PORT}
${DATA_ROOT}
${HLS_FRAGMENT}
${HLS_PLAYLIST_LENGTH}
${INGEST_PORT}
${INGEST_RTMP_PORT}
'

envsubst "${envs}" < /usr/local/srs/conf/srs.conf.template > /usr/local/srs/conf/srs.conf

mkdir -p "${DATA_ROOT}"/hls/stream

exec "$@"
