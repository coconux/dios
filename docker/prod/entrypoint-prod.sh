#!/bin/sh

mkdir -p "${DATA_ROOT}"/hls/stream
mkdir -p "${DATA_ROOT}"/emoji/png
mkdir -p "${DATA_ROOT}"/stickers

/dios/docker/shared/copy-emoji.sh
/dios/docker/shared/copy-poster.sh

bin/write_custom_emojos "${DATA_ROOT}"/emoji/png
if [ ! -f "${DATA_ROOT}"/stickers/stickers.json ]; then
  echo '{}' > "${DATA_ROOT}"/stickers/stickers.json
fi

exec "$@"
