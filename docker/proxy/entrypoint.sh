#!/bin/sh

# shellcheck disable=SC2016
envs='
${APP_HOST}
${APP_PORT}
${INGEST_HOST}
${INGEST_PORT}
'

envsubst "${envs}" < /etc/nginx/conf.d/proxy.conf.template > /etc/nginx/conf.d/proxy.conf

exec "$@"
