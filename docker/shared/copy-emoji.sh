#!/bin/sh

for emojo in /dios/static/emoji/*.png; do
  filename="$(basename "${emojo}")"
  if [ ! -f "${DATA_ROOT}/emoji/png/${filename}" ]; then
    cp "/dios/static/emoji/${filename}" "${DATA_ROOT}/emoji/png/${filename}"
  else
    echo "-- ${filename} already exists, skipping"
  fi
done
