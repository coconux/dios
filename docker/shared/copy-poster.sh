#!/bin/sh

mkdir -p "${DATA_ROOT}/images"

if [ ! -f "${DATA_ROOT}/images/poster.jpg" ]; then
    cp "/dios/static/images/poster.jpg" "${DATA_ROOT}/images/poster.jpg"
else
  echo "-- poster.jpg already exists, skipping"
fi
