# architecture

The architecture of DIOS has undergone several significant reconfigurations, but
this document will attempt to reflect the current state of the software.

![DIOS architecture](./architecture.jpg)

DIOS is not a monolithic application; rather it is made up of several
components:

- a **web application** written in [Rust](https://www.rust-lang.org/) that provides
  the user interface, chat server, and access control
- an instance of [SRS (Simple Realtime Server)](https://github.com/ossrs/srs/wiki/v4_EN_Home) configured
  as an **RTMP server**
- a **PostgreSQL database** for persisting user data
- an NGINX **reverse proxy/ingress** for routing incoming requests

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [architecture](#architecture)
    - [rust web application](#rust-web-application)
        - [user model](#user-model)
        - [video player](#video-player)
        - [chat server](#chat-server)
    - [SRS as an RTMP server](#srs-as-an-rtmp-server)
        - [authentication](#authentication)
    - [PostgreSQL](#postgresql)
    - [reverse proxy/ingress](#reverse-proxyingress)

<!-- markdown-toc end -->

## rust web application

The Rust web application comprises the primary original development work of this
project; it glues the other components together and provides a custom web
interface for the stream.  As a result, “DIOS” may sometimes be used to refer
specifically to this component of the system.

The Rust code exists in the [`/src`](../src) directory of this repository.

### user model

Users in DIOS are defined as `structs` and are persisted in and queried from
PostgreSQL by the [Diesel ORM](https://diesel.rs/).  The model definition exists
in [`src/models.rs`](../src/models.rs).

When someone attempts to sign up for a user account, a new `User` object is
created, but is initially de-activated until the person signing up completes the
email confirmation process.  (Email confirmation can be disabled by setting the
`SMTP_ENABLED` environment variable to `false`, but this isn’t recommended in
production.)

Only users with the `streamer` property set to `true` are authorized to stream
on DIOS.

### video player

When a user streams to DIOS via [OBS](https://obsproject.com/) or similar
software, they do so over RTMP.  RTMP is a protocol, not a video format.  The
RTMP server (SRS) takes the stream from OBS and converts it to an
[HLS-compatible](https://en.wikipedia.org/wiki/HTTP_Live_Streaming) media
playlist.  These playlists (which have the `.m3u8` extension) can be loaded by
an HTML `<video>` element in a web page.  However, not all browsers currently
have native support for HLS.  Therefore we use the [`video.js`
library](https://github.com/videojs/video.js) to ensure that the playlist can be
loaded by most modern browsers.

### chat server

TODO

## SRS as an RTMP server

SRS is an open-source project that provides a video streaming server that
supports multiple protocols and formats in addition to RTMP.  However, due to
various constraints of both OBS and our own infrastructure, we currently can’t
use more modern streaming solutions such as WebRTC.

SRS also includes an HTTP server in order to provide a [basic web
player](https://ossrs.net/trunk/research/players/srs_player.html?autostart=true&server=r.ossrs.net&vhost=r.ossrs.net),
but we disable this in favor of our own stream UI.  We do, however, rely on it
to serve the HLS playlist: since video.js is a client-side library, it has to
fetch `index.m3u8` over HTTP.

### authentication

When a user attempts to stream to DIOS, we have to ensure that they are a user
with streaming privileges.  This is accomplished with HTTP hooks defined in the
SRS configuration:

```conf
# @see https://github.com/ossrs/srs/blob/4.0release/trunk/conf/full.conf#L1068
#
# APP_HOST and APP_PORT should be replaced with the host and port of the Rust
# application server
http_hooks {
  enabled       on;
  on_publish    http://${APP_HOST}:${APP_PORT}/on_publish;
  on_unpublish  http://${APP_HOST}:${APP_PORT}/on_unpublish;
}
```

When a user attempts to begin streaming, SRS sends a request to the
`/on_publish` endpoint of the Rust server.  This request includes the users’
(private) stream key, which DIOS uses to look up the user and verify whether or
not they are allowed to stream.  If they are, the stream begins; if not, the
connection is closed.

When a user ends their stream, SRS sends a request to `/on_unpublish`.  This
tells DIOS to mark them as offline.

## PostgreSQL

PostgreSQL is used only for persisting user data.  The Rust
application code interfaces with it using the Diesel ORM.  SRS does not interact
with PostgreSQL.

As of now we have no way, in the web interface, to manage which users have
permission to stream.  Instead it is handled with a shell script that is run on
the Rust server itself:

```shell
/dios/scripts/admin.sh 'user@email.com'   # grant streaming permissions
/dios/scripts/unadmin.sh 'user@email.com' # revoke streaming permissions
```

## reverse proxy/ingress

In our currently deployment configuration, none of the above components are
accessible from the public internet.  Instead they are deployed behind an NGINX
server that routes requests from the outside to the appropriate service:

- RTMP traffic on port 1935 (configurable) are sent to the SRS ingest
- HTTP requests to `/hls` (for the the HLS playlist) are routed to the SRS HTTP
  server
- all other HTTP traffic is sent to the Rust application server
- no outside access to PostgreSQL is allowed

The architecture diagram from above, again:

![DIOS architecture](./architecture.jpg)
