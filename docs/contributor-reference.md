# contributor’s reference

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [contributor’s reference](#contributors-reference)
    - [local setup](#local-setup)
        - [without docker](#without-docker)
        - [with docker](#with-docker)
            - [using docker for services only](#using-docker-for-services-only)
    - [components](#components)
        - [postgres](#postgres)
        - [ffmpeg](#ffmpeg)
    - [architecture](#architecture)
        - [actors and messages](#actors-and-messages)
        - [emoji](#emoji)
            - [adding and removing emoji](#adding-and-removing-emoji)
        - [stickers](#stickers)
            - [adding and removing stickers](#adding-and-removing-stickers)

<!-- markdown-toc end -->

## local setup

### without docker
1. `cp .env.template to .env.`
1. set up a local PostgreSQL instance and modify the `POSTGRES_*` variables in
`.env` appropriately.
1. `npm i`
1. `npx webpack --watch --config webpack.config.js` (either in the background or
   in a separate terminal)
1. initial emoji setup:
    1. `mkdir -p ${DATA_ROOT}/emoji/png`
    1. `cp static/emoji/*.png ${DATA_ROOT}/emoji/png/`
    1. `cargo run --bin write_custom_emojos static/emoji`
1. `cargo install systemfd cargo-watch` (for autoreloading; see
   <https://actix.rs/docs/autoreload/>)
1. `scripts/server.sh`

### with docker

To launch DIOS in a local development environment, run
```
docker-compose up
```

Run `docker-compose build <service>` to rebuild one of the services defined in
`docker-compose.yaml`, or use `docker-compose up --build` to rebuild
everything.  Changes to the Rust code should trigger a recompile without having
to do this.

#### using docker for services only

If you're debugging DIOS or if docker is slow, you may not want the server and
rust compilation to run in a container, but you can still run npm and postgres
using docker-compose:

1. `cp .env.template to .env.`
1. Update `POSTGRES_HOST` to localhost and `DATA_ROOT` to a folder, e.g., `./data`.
1. initial emoji setup:
    1. `mkdir -p ${DATA_ROOT}/emoji/png`
    1. `cp static/emoji/*.png ${DATA_ROOT}/emoji/png/`
    1. `cargo run --bin write_custom_emojos static/emoji`
1. run `docker-compose start assets db`.
1. in a different terminal, start cargo:
    ```
    cargo install systemfd cargo-watch
    systemfd --no-pid -s http::5000 -- cargo watch -x run
    ```

## components

![DIOS architecture](./architecture.jpg)

A detailed walkthrough of the system architecture is in
[`docs/architecture.md`](./architecture.md).

### postgres

The application talks to the PostgreSQL database via
[Diesel](https://diesel.rs/).  Existing migrations will be automatically run on
startup, but if you add new ones you will have to perform a manual migration:

```shell
cargo install diesel_cli --no-default-features --features postgres
diesel migration generate new_migration
diesel migration run
```

### ffmpeg

We use a slightly modified build of ffmpeg that allows us basic access control;
future work will involve allowing DIOS to use official builds.

## architecture

### actors and messages

The web app part of DIOS is organized into actors, which communicate via messages.  This
approach allows for asychronous handling of multiple users, although you don't need to
deeply understand the concurrency model to work with this code—the Actix API and
Rust's type system help ensure correctness.

Before we go into specifics, there are two important clarifications to make.
First, "actors." Some actor and message systems allow for actors to subscribe to
messages of a particular type. In Actix (and thus DIOS), the sender actor decides which actors will receive the message, sometimes called a push model.
However, Actix actors can maintain a list of actors and broadcast a message to
all of them.

Second, "message." Unfortunately, we have three things in DIOS we could call "messages." There are the
messages users see in chat ("chat messages"), the messages sent between the
client and server ("websocket messages"), and the messages used to communicate
between actors ("Actix messages"). We will use "messages" to refer exclusively
to "Actix messages" in this section, following the convention we try to maintain
in the code base.

Imagine a streamer wants to set the stream title, so they type `/title Sunday
Stream` and click Send. After a few seconds, they see the header "Sunday
Stream." How does this work behind the scenes? When the streamer connected, the
server created a `WsChatSession`, which is an Actix actor that acccepts
websocket messages and has the address of the central `ChatServer`. The streamer
clicking Send causes the following to happen:

1. JavaScript code in the streamer's browser sent the websocket message `/title Sunday Stream` over that websocket connection.
1. The `WsChatSession` actor reads the text string, looking for a `/` character.
   Finding it in `/title Sunday Stream`, it creates a `TitleMessage` within an
   `Envelope`, a structure with generic message metadata, including the user associated with
   the message.
1. The `WsChatSession` sends the message to the `ChatServer`.
1. the `ChatServer` validates that the user who sent the message is in fact a
   streamer, by checking the database.
1. The `ChatServer` updates its title.
1. During the next regular update, a `MetaMessage` is sent with the latest
   title.

Most actions a user takes follows a similar pattern:

1. The client JavaScript sends a websocket message (occasionally it POSTs a form).
1. The `WsChatSession` converts it to an Actix message.
1. The `ChatServer` validates that the user can take that action.
1. If valid, the `ChatServer` performs the action.
1. The client receives a message with the new state, e.g., a new chat
   message or the current title and viewer count, and the JavaScript updates the
   page.

You'll almost always want to create a new message within an `Envelope` to
include metadata about the message.

While
there's nothing in the Actix model that requires a message to respond directly
to a user action or a websocket message, in DIOS there's often a one-to-one mapping between a user action and a message being sent. One exception is the `MetaMessage`, which is generated automatically and updates all of the clients. This is done on a regular interval, rather than being requested by the user.

There are two major kinds of actor:
* `ChatServer` is a singleton actor that authenticates messages and acts on them (see `src/chat/server.rs`).
* `WsChatSession` actors interpret messages from the users' browsers (see
    `src/chat/sessions.rs`).

The various kind of messages, plus the `Envelope` wrapper struct, are defined in `src/messages.rs`.

For a code example, you can refer to [MR !312](https://gitlab.com/egirls/dios/-/merge_requests/312), which adds the `\clear` command.

### emoji

The [picker widget](https://github.com/joeattardi/emoji-button) we use loads a
JSON file via URL; the JSON is an array of hashes with a `name` field
corresponding to the emoji shortcode, and an `emoji` field corresponding to the
URL of the image file:

```json
[
  {
    "name": "AYAYA",
    "emoji": "/emoji/png/AYAYA.png"
  },
  {
    "name": "BESHT",
    "emoji": "/emoji/png/BESHT.png"
  },
  {
    "name": "PISS",
    "emoji": "/emoji/png/PISS.png"
  }
]
```

When you select an emoji from the picker, it inserts `:${name}:` into the
text field, meaning that if you know the shortcode, you can type it manually
instead.  This logic is in `static/js/emoji-picker.js`.  The rest of the text
processing, where the shortcodes are replaced with the images and rendered in
the chat window, happens in `static/js/chat.js` (see specifically the
`render_emoji` function).

Because the list of emoji is a JSON file stored on disk, any additions or
deletions require the JSON file to be regenerated, and for users to refresh the
page in order to load the new list of emoji.  When the application is first run,
the initial list is generated by the `write_custom_emojos` script, which can be
run manually during development with `cargo run --bin write_custom_emojos
static/emoji`.

#### adding and removing emoji

Users authorized as streamers can add and delete emoji from their profile page.
The following steps occur when a user selects a file from their machine and
attempts to uploads it:

1. A client-side check is made on the size of the image; if it is above 250kb,
   the 'upload' button is disabled
1. The `add_emoji` handler in `src/routes/profile/emoji.rs` performs several
   operations:
   - If the image is not a PNG, the upload is rejected and the user is presented
     with an appropriate error message
   - A valid shortcode is generated from the filename:
     `emojo::make_allowed_shortcode` strips whitespace and non-ASCII characters,
     and if the resulting string already exists as the shortcode for another
     emoji, a random string is generated instead (that the user can change after
     upload)
   - The image is saved to `${DATA_ROOT}/emoji/png/${name}.png`
   - If, despite the client-side check above, an image larger than 250kb is
     being uploaded, another size check is performed
   - A new version of the JSON file is written by `emojo::update_emojo_list`
     (see `src/emojo.rs`): the contents of `${DATA_ROOT}/emoji/png/${name}.png`
     are read, and the JSON is constructed by using the filenames as shortcodes
   - The next time a user loads the chat, the new JSON is loaded, and the emoji
     is available for use

When an emoji is renamed or deleted, a similar process occurs: the image file on
disk is either renamed or deleted, and the JSON file is then regenerated to
reflect the change.

### stickers

Stickers are handled similarly to custom emoji. The JSON file looks a little
different because the stickers are arranged into packs. The top of the file is a
hash with each pack's name as the key and a list of each pack's contents as the
value. Each item of the pack follows a similar format:

```json
{"dune": [{"name":"godemperor", "url":"stickers/dune/png/godemperor.png"},
	{"name":"godemperor2", "url":"stickers/dune/png/godemperor2.png"}]}
```

#### adding and removing stickers

Currently, there's no way to add stickers directly. Instead, stickers are added
as emoji, then renamed according to the pattern `<pack>/<sticker>`.

When stickers are renamed, they're stored into
`${DATA_ROOT}/stickers/${pack}/png/${name}.png` and the sticker file is
regenerated by `stickers::update_sticker_list`.
