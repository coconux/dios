CREATE TABLE password_resets (
  id VARCHAR PRIMARY KEY,
  email VARCHAR(150) NOT NULL,
  expires_at TIMESTAMP NOT NULL
)
