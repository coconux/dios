CREATE TABLE server_about (
  id VARCHAR PRIMARY KEY,
  welcome_message VARCHAR(50) DEFAULT 'hello and welcome',
  blurb VARCHAR(200)
);
