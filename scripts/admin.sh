#!/bin/sh

EMAIL="$1"

if [ -z ${EMAIL+x} ]; then
  echo "--- ERROR: no email address specified"
  exit 1
fi

PG_URL="postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}/${POSTGRES_DB}"

stream_key=$(uuidgen | tr -d '\n')

psql "${PG_URL}" \
     -c "UPDATE users SET streamer = 'true', stream_key = '${stream_key}' WHERE email = '${EMAIL}'"

echo "-- Congratulations you can now stream with key ${stream_key}"
