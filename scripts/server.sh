#!/bin/sh

systemfd --no-pid -s 0.0.0.0:8080 -- \
         cargo watch -x run \
         -i .gitlab \
         -i .gitlab-ci.yml \
         -i .jshintrc \
         -i chart \
         -i ci \
         -i docker \
         -i docker-compose.yaml \
         -i docs \
         -i nginx \
         -i readme.md \
         -i scripts \
         -i static
