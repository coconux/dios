use regex::Regex;
use std::env;
use std::fs;
use std::io::{Error, ErrorKind};
use std::path::Path;
use std::str;

use crate::inlines::{CustomInline,Sticker};

pub fn make_json_from_emojos(
    emojos: Vec<String>,
) -> String {
    let blobs: Vec<String> = emojos
        .iter()
        .map(|emoj| format!("{{\"name\":\"{}\",\"emoji\":\"/emoji/png/{}.png\"}}",
                            emoj.clone(),
                            emoj.clone())).collect();

    format!("[{}]", blobs.join(","))
}

pub fn emojos(path: &Path) -> Vec<Option<String>> {
    let dir_contents = match fs::read_dir(path) {
        Ok(contents) => contents,
        Err(e) => panic!(
            "Bad path {}: {}",
            path.to_str().unwrap(),
            e
        )
    };

    dir_contents
        .map(|p| emojo_from_path(&p.unwrap().path()) )
        .collect()
}

pub fn emojo_from_path(path: &Path) -> Option<String> {
    if path.is_dir() {
        return None;
    }

    if path.extension().is_none() {
        return None;
    }

    if path.extension().unwrap() != "png" {
        return None;
    }

    let filename = path
        .file_name()
        .unwrap()
        .to_str()
        .unwrap();

    Some(str::replace(filename, ".png", ""))
}

pub fn emojos_from_path(base: &str) -> Vec<String> {
    let maybe_emojos = emojos(&Path::new(base));

    let mut sorted_emojos = maybe_emojos.iter()
        .filter(|opt| opt.is_some())
        .map(|emoj| emoj.clone().unwrap())
        .collect::<Vec<String>>();

    sorted_emojos.sort();
    sorted_emojos
}

pub fn update_emojo_list(png_dir: &str) -> bool {
    let source_dir = Path::new(png_dir);
    if !source_dir.is_dir() {
        panic!(
            "Provided path {} is not a directory",
            source_dir.to_str().unwrap()
        )
    }
    let emojos = emojos_from_path(&source_dir.to_str().unwrap());
    let json = make_json_from_emojos(emojos);

    let data_root = match env::var("DATA_ROOT") {
        Ok(val) => val,
        Err(_) => String::from("/data"),
    };
    let custom_json = format!("{}/emoji/custom.json", data_root);
    match fs::write(&custom_json, json) {
        Ok(_) => {
            debug!("wrote {}", &custom_json);
            true
        },
        Err(e) => panic!("Failed to write {}: {:?}", &custom_json, e)
    }
}

pub fn make_allowed_shortcode (shortcode: &str) -> String {
    let reggie = Regex::new("[^a-zA-Z0-9]").unwrap();

    reggie.replace_all(shortcode, "").to_string()
}

pub fn rename_emojo(
    old_name: &CustomInline,
    new_name: &str,
) -> Result<(), std::io::Error> {
    let data_root = match env::var("DATA_ROOT") {
        Ok(val) => val,
        Err(_) => String::from("/data"),
    };

    let cleaned_newname = make_allowed_shortcode(new_name);

    // let old = &format!("{}/emoji/png/{}.png", &data_root, old_name);
    let old = match old_name {
        CustomInline::Emojo(e) => format!("{}/emoji/png/{}.png", &data_root, e),
        CustomInline::Sticker(s) =>  format!("{}/stickers/{}/png/{}.png", &data_root, s.pack, s.name) 

    };
    let new = &format!("{}/emoji/png/{}.png", &data_root, cleaned_newname);

    let old_path = Path::new(&old);
    let new_path = Path::new(new);

    if !old_path.is_file() {
        warn!("{:?} is not file", &old_path);
        return Err(Error::new(ErrorKind::InvalidInput, "not a file"))
    }
    if new_path.exists() {
        warn!("{:?} already exists", &new_path);
        return Err(Error::new(ErrorKind::AlreadyExists, "won't overwrite existing file"))
    }

    match fs::rename(old_path, new_path) {
        Ok(_) => Ok(()),
        Err(e) => {
            error!("cant remove {:?}", e);
            Err(e)
        },
    }
}



pub fn delete_emojo(name: &str) -> bool {
    let data_root = match env::var("DATA_ROOT") {
        Ok(val) => val,
        Err(_) => String::from("/data"),
    };

    let path = &format!("{}/emoji/png/{}.png", &data_root, name);
    let emojo = Path::new(path);

    if !emojo.is_file() {
        warn!("{:?} is not file", &emojo);
        return false;
    }
    match fs::remove_file(emojo) {
        Ok(_) => true,
        Err(e) => {
            warn!("cant remove {:?}", e);
            false
        },
    }
}
