
use serde::Serialize;


#[derive(Debug, PartialEq)]
pub enum CustomInline <'a> {
    Emojo(&'a str),
    Sticker(Sticker)
}

#[derive(Serialize, Debug, PartialEq)]
pub struct Sticker {
    pub pack: String,
    pub name: String
}


impl CustomInline <'_> {
    pub fn from_string(s: &str) -> Option<CustomInline> {
        let parts: Vec<&str> = s.split("/").collect();

        if parts.len() == 2 {
            Some(CustomInline::Sticker(Sticker { pack: parts[0].to_string(), name: parts[1].to_string()}))
        }
        else if parts.len() == 1  {
            Some(CustomInline::Emojo(parts[0]))
        }
        else {
            None
        }
    }
}

impl Sticker {
    pub fn from_string(s: &str) -> Option<Sticker> {

        let parts: Vec<&str> = s.split("/").collect();

        if parts.len() == 2 {
            Some(Sticker { pack: parts[0].to_string(), name: parts[1].to_string()})
        }
        else {
            None
        }
    }
}
