#[macro_use] extern crate diesel;
#[macro_use] extern crate lazy_static;
#[macro_use] extern crate log;
extern crate actix_files;
extern crate argon2;
extern crate dotenv;
extern crate subprocess;
extern crate htmlescape;

use actix::*;
use actix_identity::*;
use actix_files::Files;
use actix_session::{CookieSession};
use actix_web::{web, middleware, App, HttpServer};
use dotenv::dotenv;
use env_logger;
use listenfd::ListenFd;
use std::env;
use std::path::Path;

use diesel::prelude::*;
use diesel::r2d2::{self, ConnectionManager};
#[macro_use] extern crate diesel_migrations;
embed_migrations!();

pub mod utils {
    pub mod mail;
    pub mod markdown;
    pub mod session;
    pub mod user;
}

pub mod inlines;
pub mod emojo;
pub mod stickers;
pub mod messages;
pub mod models;
pub mod schema;
pub mod stream_info;
pub mod templates;

pub mod chat {
    pub mod server;
    pub mod sessions;
}

pub mod routes {
    pub mod auth;
    pub mod ingest_hooks;
    pub mod public;
    pub mod reset;
    pub mod util;

    pub mod profile {
        pub mod emoji;
        pub mod poster;
        pub mod profile;
        pub mod server_settings;
        pub mod user_settings;
    }
}

type ConnectionPool = r2d2::Pool<ConnectionManager<PgConnection>>;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();

    env_logger::Builder::from_env(
        env_logger::Env::default().default_filter_or("info")
    ).init();

    // HOST and PORT set the bind address
    let host_env = "HOST";
    let host = match env::var(host_env) {
        Ok(val) => val,
        Err(_) => String::from("localhost"),
    };
    let port_env = "PORT";
    let port = match env::var(port_env) {
        Ok(val) => val,
        Err(_) => String::from("8080"),
    };
    let bind_address = format!("{}:{}", host, port);

    // where to look for the stream manifests created by ffmpeg, and emoji data
    let data_root = match env::var("DATA_ROOT") {
        Ok(val) => val,
        Err(_) => String::from("/data"),
    };

    // for whether or not to use secure cookies
    let is_https = match env::var("HTTPS") {
        Ok(val) => val == "true" || val == "1" ,
        Err(_) => false,
    };

    let pg_db = env::var("POSTGRES_DB").expect("POSTGRES_DB unset");
    let pg_host = env::var("POSTGRES_HOST").expect("POSTGRES_HOST unset");
    let pg_pass = env::var("POSTGRES_PASSWORD").expect("POSTGRES_PASSWORD unset");
    let pg_user = env::var("POSTGRES_USER").expect("POSTGRES_USER unset");
    let database_url = format!("postgres://{}:{}@{}/{}",
                               &pg_user,
                               &pg_pass,
                               &pg_host,
                               &pg_db,);

    let manager = ConnectionManager::<PgConnection>::new(&database_url);
    let pool = r2d2::Pool::builder()
        .build(manager)
        .expect("Failed to create pool.");

    let conn = pool.get().expect("couldn't get db connection from pool");
    embedded_migrations::run_with_output(
        &conn,
        &mut std::io::stdout()
    ).expect("Failed to run migrations");

    let server_state = web::Data::new(stream_info::StreamInfo::default());

    //for now, use a separate pool for the ChatServer
    let server = chat::server::ChatServer::new(server_state.clone(), pool.clone()).start();

    let mut server = HttpServer::new(move || {
        App::new()
            .data(pool.clone())
            .data(server.clone())
            .app_data(server_state.clone())
            .wrap(IdentityService::new(
                CookieIdentityPolicy::new(&env::var("AUTH_KEY").expect("AUTH_KEY must be set").as_bytes())
                    .name("auth-cookie")
                    .secure(is_https)
            ))
            .wrap(CookieSession::signed(&env::var("SESSION_KEY").expect("SESSION_KEY must be set").as_bytes()).secure(is_https))
            .wrap(middleware::Logger::default())
            .service(Files::new("/emoji/", Path::new(&data_root).join("emoji/")))
            .service(Files::new("/stickers/", Path::new(&data_root).join("stickers/")))
            .service(Files::new("/images/", Path::new(&data_root).join("images/")))
            .service(Files::new("/static/", "static/"))
            .service(web::resource("/ws/").to(chat::server::chat_route))
            .service(routes::auth::activate)
            .service(routes::auth::login)
            .service(routes::auth::logout)
            .service(routes::auth::signup)
            .service(routes::ingest_hooks::on_connect)
            .service(routes::ingest_hooks::on_unpublish)
            .service(routes::profile::emoji::add_emoji)
            .service(routes::profile::emoji::delete_emoji)
            .service(routes::profile::emoji::delete_sticker)
            .service(routes::profile::emoji::rename_emoji)
            .service(routes::profile::poster::upload_poster)
            .service(routes::profile::profile::profile)
            .service(routes::profile::server_settings::update_server_info)
            .service(routes::profile::user_settings::change_stream_key)
            .service(routes::profile::user_settings::change_username)
            .service(routes::public::chat)
            .service(routes::public::index)
            .service(routes::public::reset)
            .service(routes::public::video)
            .service(routes::reset::new_password)
            .service(routes::reset::new_password_callback)
            .service(routes::reset::reset)
    });

    // https://actix.rs/docs/autoreload/
    let mut listenfd = ListenFd::from_env();
    server = if let Some(l) = listenfd.take_tcp_listener(0).unwrap() {
        server.listen(l)?
    } else {
        server.bind(bind_address)?
    };

    let res = server.run().await;

    res
}
