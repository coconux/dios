use super::schema::*;

#[derive(Debug, Clone, Insertable, Queryable,)]
#[table_name="confirmations"]
pub struct Confirmation {
    // make sure these are listed in the same order as in schema.rs!
    //
    // yes, that's batshit!!
    pub id: String,
    pub email: String,
    pub expires_at: chrono::NaiveDateTime,
}

#[derive(Debug, Clone, Insertable, Queryable,)]
#[table_name="password_resets"]
pub struct PasswordReset {
    // make sure these are listed in the same order as in schema.rs!
    //
    // yes, that's batshit!!
    pub id: String,
    pub email: String,
    pub expires_at: chrono::NaiveDateTime,
}

#[derive(Debug, Clone, Insertable, Queryable,)]
#[table_name="server_about"]
pub struct ServerAbout {
    // make sure these are listed in the same order as in schema.rs!
    //
    // yes, that's batshit!!
    pub id: String,
    pub welcome_message: Option<String>,
    pub blurb: Option<String>,
}

#[derive(Debug, Clone, Insertable, Queryable, Identifiable)]
#[table_name="users"]
pub struct User{
    // make sure these are listed in the same order as in schema.rs!
    //
    // yes, that's batshit!!
    pub id: String,
    pub username: String,
    pub password_hash: String,
    pub moderator: bool,
    pub streamer: bool,
    pub email: String,
    pub activated: bool,
    pub indefinitely_banned: bool,
    pub last_namechange: chrono::NaiveDateTime,
    pub stream_key: Option<String>,
    pub live: bool,
}
