use actix_web::{web, get, post, HttpRequest, HttpResponse, Error};
use actix_http::http;
use actix_identity::*;
use actix_session::Session;
use argon2;
use diesel::prelude::*;
use serde::Deserialize;
use std::env;

use crate::ConnectionPool;
use crate::models::*;
use crate::schema::confirmations;
use crate::schema::users::dsl::*;
use crate::utils::mail::*;
use crate::utils::session::*;
use crate::utils::user::*;

#[derive(Deserialize)]
pub struct LoginForm<>{
    chatonly: String,
    email: String,
    password: String,
}

#[derive(Deserialize)]
pub struct SignupForm<>{
    chatonly: String,
    email: String,
    password: String,
    username: String,
}

#[derive(Deserialize)]
pub struct LogoutForm<>{
    chatonly: String,
}

#[post("/signup")]
pub async fn signup(
    form: web::Form<SignupForm>,
    identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> Result<HttpResponse, Error>{
    let connection = pool.get().expect("couldn't get db connection from pool");

    let mut redirect_location = "/";
    if form.chatonly == "true" {
        redirect_location = "/chat";
    };

    let cleaned_user: String = form.username
        .chars()
        .filter(|c| is_allowed_char(c) )
        .collect();

    let username_limit = match env::var("MAX_USERNAME_LENGTH") {
        Ok(val) => val.parse::<usize>().unwrap_or(20),
        Err(_) => 20,
    };

    if cleaned_user.len() > username_limit {
        flash(
            &session,
            "error",
            &format!("username must be {} characters or less", username_limit)
        );

        return Ok(HttpResponse::SeeOther()
                  .set_header(http::header::LOCATION, redirect_location)
                  .finish())
    }

    match get_user_by_email(&form.email, &connection) {
        // db query success
        Ok(Some(found_user)) => signup_existing_user(found_user, &connection, &session),
        Ok(None) => {
            signup_new_user(
                &cleaned_user,
                &form.email,
                &form.password,
                &connection,
                &session,
                &identity,
            )
        },
        Err(e) => panic!("Failed to query DB for email {}: {:?}", &form.email, e)
    }
    Ok(HttpResponse::SeeOther()
       .set_header(http::header::LOCATION, redirect_location)
       .finish())

}

#[get("/activate")]
pub async fn activate(
    req: HttpRequest,
    identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> Result<HttpResponse, Error> {
    let connection = pool.get().expect("couldn't get db connection from pool");

    let query: Vec<&str> = req.query_string().split('=').collect();
    let querycode = query.get(1);
    if querycode == None {
        return Ok(HttpResponse::SeeOther()
                  .set_header(http::header::LOCATION, "/")
                  .finish())
    }
    let confirmation_code = querycode.unwrap();

    let find_conf = confirmations::table
        .filter(confirmations::id.eq(confirmation_code))
        .first::<Confirmation>(&connection)
        .optional()
        .unwrap();

    if let Some(found_conf) = find_conf {
        let now = chrono::Utc::now().naive_utc();

        if found_conf.expires_at > now {
            diesel::update(users.filter(email.eq(&found_conf.email)))
                .set(activated.eq(true))
                .execute(&connection)
                .unwrap();

            diesel::update(
                confirmations::table
                    .filter(confirmations::id.eq(confirmation_code)))
                .set(confirmations::expires_at.eq(now))
                .execute(&connection)
                .unwrap();

            let find_user = users
                .filter(email.eq(&found_conf.email))
                .first::<User>(&connection)
                .optional()
                .unwrap();

            if let Some(found_user) = find_user {
                identity.remember(found_user.username.to_owned());
            } else {
                flash(
                    &session,
                    "error",
                    "no user found with this email address"
                );
            }
        } else {
            flash(
                &session,
                "error",
                "invalid confirmation code"
            );
        }
    } else {
        flash(
            &session,
            "error",
            "invalid confirmation code"
        );
    }

    Ok(HttpResponse::SeeOther()
       .set_header(http::header::LOCATION, "/")
       .finish())
}

#[post("/login")]
pub async fn login(
    form: web::Form<LoginForm>,
    identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>
) -> Result<HttpResponse, Error>{
    let connection = pool.get().expect("couldn't get db connection from pool");

    let password = form.password.to_owned();

    let mut redirect_location = "/";
    if form.chatonly == "true" {
        redirect_location = "/chat";
    };

    match get_user_by_email(&form.email, &connection) {
        Ok(Some(found_user)) => {
            if ! &found_user.activated {
                let new_confirmation = create_confirmation_for(
                    &found_user.email,
                    &connection,
                );
                send_confirmation_email(new_confirmation);
                flash(
                    &session,
                    "warning",
                    "user is not activated; re-sending confirmation email"
                );
            } else if found_user.indefinitely_banned {
                flash(
                    &session,
                    "error",
                    "no user exists with that email address."
                );

            } else if argon2::verify_encoded(&found_user.password_hash, password.as_bytes()).unwrap() {
                identity.remember(found_user.username.to_owned());
            } else {
                flash(
                    &session,
                    "error",
                    "incorrect password."
                );
            }
        },
        Ok(None) => {
            flash(
                &session,
                "error",
                "no user exists with that email address."
            );
        },
        Err(e) => panic!("Failed to query DB for email {}: {:?}", &form.email, e),
    }
    Ok(HttpResponse::SeeOther()
       .set_header(http::header::LOCATION, redirect_location)
       .finish())
}

#[post("/logout")]
pub async fn logout(form: web::Form<LogoutForm>, identity: Identity) -> HttpResponse {
    let mut redirect_location = "/";
    if form.chatonly == "true" {
        redirect_location = "/chat";
    };

    identity.forget();
    HttpResponse::SeeOther()
        .set_header(http::header::LOCATION, redirect_location)
        .finish()
}
