use actix_web::{web, post, HttpResponse};
use actix_identity::*;
use actix_session::Session;
use diesel::prelude::*;
use serde::Deserialize;

use crate::ConnectionPool;
use crate::models::*;
use crate::schema::users::dsl::*;
use crate::utils::user::user_has_stream_powers;

// form params https://github.com/ossrs/srs/blob/4.0release/trunk/conf/full.conf#L1070
#[derive(Deserialize)]
pub struct StreamPublish {
    stream: String,
}

#[post("/on_connect")]
pub async fn on_connect (
    stream_publish: web::Json<StreamPublish>,
    _identity: Identity,
    _session: Session,
    pool: web::Data<ConnectionPool>,
) -> HttpResponse {
    let connection = pool.get().expect("couldn't get db connection from pool");

    debug!("Got stream key {}", &stream_publish.stream);

    let new_streamer = users
        .filter(stream_key.eq(&stream_publish.stream))
        .first::<User>(&connection)
        .optional()
        .unwrap();

    if let Some(found_user) = new_streamer {
        if !user_has_stream_powers(&found_user) {
            return HttpResponse::Forbidden().finish()
        }

        let already_live = users
            .filter(live.eq(true))
            .first::<User>(&connection)
            .optional()
            .unwrap();

        if let Some(og) = already_live {
            if og.email != found_user.email {
                return HttpResponse::ServiceUnavailable().finish()
            }
        }

        debug!("Authorized {} to stream", &found_user.username);

        diesel::update(&found_user)
            .set(live.eq(true))
            .execute(&connection)
            .unwrap();

        // @see https://github.com/ossrs/srs/blob/4.0release/trunk/conf/full.conf#L1083-L1084
        return HttpResponse::Ok()
            .body("0")

    }
    HttpResponse::Forbidden()
        .body("1")
}

#[post("/on_unpublish")]
pub async fn on_unpublish (
    stream_publish: web::Json<StreamPublish>,
    _identity: Identity,
    _session: Session,
    pool: web::Data<ConnectionPool>,
) -> HttpResponse {
    let connection = pool.get().expect("couldn't get db connection from pool");

    debug!("Got stream key {}", &stream_publish.stream);

    let streaming_user = users
        .filter(stream_key.eq(&stream_publish.stream))
        .first::<User>(&connection)
        .optional()
        .unwrap();

    if let Some(found_user) = streaming_user {
        debug!("{} going offline", &found_user.username);

        diesel::update(&found_user)
            .set(live.eq(false))
            .execute(&connection)
            .unwrap();
    }
    HttpResponse::Ok().body("0")
}
