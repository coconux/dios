use actix_http::http;
use actix_identity::*;
use actix_multipart::Multipart;
use actix_session::Session;
use actix_web::{web, post, HttpResponse};
use futures::{StreamExt, TryStreamExt};
use serde::Deserialize;
use std::env;
use std::fs::File;
use std::io::Write;
use std::path::Path;
use uuid::Uuid;

use crate::ConnectionPool;
use crate::emojo;
use crate::stickers;
use crate::routes::util::*;
use crate::utils::session::*;
use crate::utils::user::*;
use crate::inlines;

#[derive(Deserialize)]
pub struct DeleteEmojiForm<>{
    shortcode: String,
}

#[derive(Deserialize)]
pub struct RenameEmojiForm<>{
    old_name: String,
    new_name: String,
}


#[post("/add_emoji")]
pub async fn add_emoji(
    mut payload: Multipart,
    identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> HttpResponse {
    if let Some(iden) = identity.identity() {
        let connection = pool.get().expect("couldn't get db connection from pool");

        if let Some(u) = get_user_by_name(&iden, &connection) {
            if !user_has_stream_powers(&u) {
                return HttpResponse::Forbidden()
                    .set_header(http::header::LOCATION, "/")
                    .finish()
            }

            let data_root = match env::var("DATA_ROOT") {
                Ok(val) => val,
                Err(_) => String::from("/data"),
            };

            while let Ok(Some(mut field)) = payload.try_next().await {
                if field.content_type().essence_str() != "image/png" {
                    flash(
                        &session,
                        "error",
                        "sorry we only allow png"
                    );

                    return HttpResponse::SeeOther()
                        .set_header(http::header::LOCATION, "/profile")
                        .finish()
                }

                let dispo = field.content_disposition().unwrap();
                let filename = emojo::make_allowed_shortcode(
                    &str::replace(
                        dispo
                            .get_filename()
                            .unwrap(),
                        ".png", "")
                );

                let mut filepath = format!(
                    "{}/emoji/png/{}.png",
                    data_root,
                    &filename,
                );

                if Path::new(&filepath).exists() {
                    let newname = emojo::make_allowed_shortcode(
                        &Uuid::new_v4().to_string()
                    );

                    filepath = format!(
                        "{}/emoji/png/{}.png",
                        data_root,
                        &newname,
                    );
                }

                let mut file = match File::create(&filepath) {
                    Ok(file) => file,
                    Err(e) => panic!("couldn't create file {}: {}", &filepath, e.to_string()),
                };

                let mut filesize: usize = 0;
                let mut data: Vec<actix_web::web::Bytes> = Vec::new();

                while let Some(chunk) = field.next().await {
                    let blob = chunk.unwrap();
                    filesize += &blob.len();

                    if filesize > 250000 {
                        flash(
                            &session,
                            "error",
                            "the emojo is too large"
                        );

                        return HttpResponse::SeeOther()
                            .set_header(http::header::LOCATION, "/profile")
                            .finish()
                    }
                    data.push(blob);
                }

                web::block(
                    move || {
                        let bips = data.iter().map(|b| b.as_ref() ).collect::<Vec<&[u8]>>();
                        file.write_all(&bips.concat())
                    }
                ).await.unwrap();
            }

            if emojo::update_emojo_list(&format!("{}/emoji/png", data_root)) {
                flash(
                    &session,
                    "info",
                    "emojo added"
                );

            } else {
                flash(
                    &session,
                    "error",
                    "the emojo was too strong to be added, please speak with a higher power"
                );
            }

            return HttpResponse::SeeOther()
                .set_header(http::header::LOCATION, "/profile")
                .finish()
        } else {
            return HttpResponse::SeeOther()
                .set_header(http::header::LOCATION, "/")
                .finish()
        }
    } else {
        return HttpResponse::SeeOther()
            .set_header(http::header::LOCATION, "/")
            .finish()
    }
}

#[post("/rename_emoji")]
pub async fn rename_emoji (
    form: web::Form<RenameEmojiForm>,
    identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> HttpResponse {
    as_streamer(identity, pool, |_user, _conn| {
        let data_root = match env::var("DATA_ROOT") {
            Ok(val) => val,
            Err(_) => String::from("/data"),
        };

        let old_inline = inlines::CustomInline::from_string(&form.old_name).unwrap();
        let png_dir = format!("{}/emoji/png", data_root);

        match inlines::CustomInline::from_string(&form.new_name) {
            Some(inlines::CustomInline::Emojo(_)) => { //Emojo

                match emojo::rename_emojo(&old_inline, &form.new_name) {
                    Ok(_) => {
                        flash(
                            &session,
                            "info",
                            "emojo renamed"
                        );
                    },
                    Err(e) => {
                     warn!("could not rename emojo: {:?} ", e);
                        flash(
                            &session,
                            "error",
                            "the emojo was too strong to be renamed, please speak with a higher power"
                        );
                    }
                }
            }
            Some(inlines::CustomInline::Sticker(sticker)) => { // Sticker
                match stickers::rename_sticker(&old_inline, &sticker) {
                    Ok(_) => {

                                flash(
                                    &session,
                                    "info",
                                    "sticker renamed"
                                )
                    }
                    Err(e) => {
                     warn!("could not rename sticker: {:?} ", e);
                                flash(
                                    &session,
                                    "error",
                                    &format!{"the sticker was too strong to be renamed, please speak with a higher power\nthis might help: {}", e}
                                );
                            }
                        }
                    },
            None => {}
        }


         if emojo::update_emojo_list(&png_dir) {
                 let sticker_dir = format!("{}/stickers", data_root);
                 if let Err(e) = stickers::update_sticker_list(&sticker_dir) {
                     warn!("could not update sticker file: {:?} ", e);
                     flash(
                         &session,
                         "error",
                         "could not update sticker file."
                     );
                 }

         }
         else {
             warn!("could not update emojo file");
             flash(
                 &session,
                 "error",
                 "could not update emojo file."
             );
         }

        HttpResponse::SeeOther()
            .set_header(http::header::LOCATION, "/profile")
            .finish()
    })
}

#[post("/delete_emoji")]
pub async fn delete_emoji (
    form: web::Form<DeleteEmojiForm>,
    identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> HttpResponse {
    as_streamer(identity, pool, |_user, _conn| {
        let data_root = match env::var("DATA_ROOT") {
            Ok(val) => val,
            Err(_) => String::from("/data"),
        };
        let png_dir = format!("{}/emoji/png", data_root);

        let success = emojo::delete_emojo(&form.shortcode) &&
            emojo::update_emojo_list(&png_dir);

        if success {
            flash(
                &session,
                "info",
                "farewell, sweet emojo"
            );
        } else {
            flash(
                &session,
                "error",
                "the emojo was too powerful to delete, please speak with a higher power"
            );
        }

        HttpResponse::SeeOther()
            .set_header(http::header::LOCATION, "/profile")
            .finish()
    })
}


#[post("/delete_sticker")]
pub async fn delete_sticker (
    form: web::Form<DeleteEmojiForm>,
    identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> HttpResponse {
    as_streamer(identity, pool, |_user, _conn| {
        let data_root = match env::var("DATA_ROOT") {
            Ok(val) => val,
            Err(_) => String::from("/data"),
        };
        let png_dir = format!("{}/stickers", data_root);

        match stickers::delete_sticker(&form.shortcode)  {
            Ok(_) => {
                match stickers::update_sticker_list(&png_dir) {
                    Ok(_) => { flash(
                            &session,
                            "info",
                            "farewell, sweet sticker"
                    );},
                    Err(_) => {
                        flash(
                            &session,
                            "error",
                            "the sticker was too powerful to delete, please speak with a higher power"
                        );
                    }
                }}
            Err(_) => {
                flash(
                    &session,
                    "error",
                    "the sticker was too powerful to delete, please speak with a higher power"
                );
            }
        }
        HttpResponse::SeeOther()
            .set_header(http::header::LOCATION, "/profile")
            .finish()
    })
}
