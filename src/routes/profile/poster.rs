use actix_http::http;
use actix_identity::*;
use actix_multipart::Multipart;
use actix_session::Session;
use actix_web::{web, post, HttpResponse};
use futures::{StreamExt, TryStreamExt};
use std::env;
use std::fs::File;
use std::io::Write;

use crate::ConnectionPool;
use crate::utils::session::*;
use crate::utils::user::*;

#[post("/upload_poster")]
pub async fn upload_poster(
    mut payload: Multipart,
    identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> HttpResponse {
    if let Some(iden) = identity.identity() {
        let connection = pool.get().expect("couldn't get db connection from pool");

        if let Some(u) = get_user_by_name(&iden, &connection) {
            if !user_has_stream_powers(&u) {
                return HttpResponse::Forbidden()
                    .set_header(http::header::LOCATION, "/")
                    .finish()
            }

            let data_root = match env::var("DATA_ROOT") {
                Ok(val) => val,
                Err(_) => String::from("/data"),
            };

            while let Ok(Some(mut field)) = payload.try_next().await {
                if field.content_type().essence_str() != "image/jpeg" {
                    flash(
                        &session,
                        "error",
                        "the offline image has to be a jpg"
                    );

                    return HttpResponse::SeeOther()
                        .set_header(http::header::LOCATION, "/profile")
                        .finish()
                }

                let filepath = format!(
                    "{}/images/poster.jpg",
                    data_root,
                );

                let mut file = match File::create(&filepath) {
                    Ok(file) => file,
                    Err(e) => {
                        flash(
                            &session,
                            "error",
                            &format!("couldn't create file {}: {}", &filepath, e.to_string())
                        );
                        return HttpResponse::BadRequest()
                            .set_header(http::header::LOCATION, "/profile")
                            .finish()
                    },
                };

                let mut filesize: usize = 0;
                let mut data: Vec<actix_web::web::Bytes> = Vec::new();

                while let Some(chunk) = field.next().await {
                    let blob = chunk.unwrap();
                    filesize += &blob.len();

                    if filesize > 1000000 {
                        flash(
                            &session,
                            "error",
                            "the image is too large"
                        );

                        return HttpResponse::SeeOther()
                            .set_header(http::header::LOCATION, "/profile")
                            .finish()
                    }
                    data.push(blob);
                }

                web::block(
                    move || {
                        let bips = data.iter().map(|b| b.as_ref() ).collect::<Vec<&[u8]>>();
                        file.write_all(&bips.concat())
                    }
                ).await.unwrap();
            }
            return HttpResponse::SeeOther()
                .set_header(http::header::LOCATION, "/profile")
                .finish()
        } else {
            return HttpResponse::SeeOther()
                .set_header(http::header::LOCATION, "/")
                .finish()
        }
    } else {
        return HttpResponse::SeeOther()
            .set_header(http::header::LOCATION, "/")
            .finish()
    }
}
