use actix_http::http;
use actix_identity::*;
use actix_session::Session;
use actix_web::{web, get, HttpRequest, HttpResponse};
use diesel::prelude::*;
use std::env;
use std::path::Path;

use crate::ConnectionPool;
use crate::emojo;
use crate::stickers;
use crate::models;
use crate::routes::util::*;
use crate::schema::server_about;
use crate::templates;
use crate::utils::user::*;

#[get("/profile")]
pub async fn profile (
    _req: HttpRequest,
    identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> HttpResponse {
    if let Some(iden) = identity.identity() {
        let connection = pool.get().expect("couldn't get db connection from pool");

        if let Some(found_user) = get_user_by_name(&iden, &connection) {
            let mut context = make_context(&session);
            context.insert("user", &iden);
            context.insert("page_title", &format!("profile for {}", &iden));

            if user_has_stream_powers(&found_user) {
                if let Ok(server_about) = server_about::table.find("1").first::<models::ServerAbout>(&connection) {
                    context.insert("welcome", &server_about.welcome_message);
                    context.insert("blurb", &server_about.blurb);
                } else {
                    context.insert("welcome", "hello and welcome");
                }

                let data_root = match env::var("DATA_ROOT") {
                    Ok(val) => val,
                    Err(_) => String::from("/data"),
                };

                let emoji = emojo::emojos_from_path(
                    &format!("{}/emoji/png", &data_root)
                );

                let stickers = stickers::stickers_for_display(
                    Path::new(&format!("{}/stickers", &data_root))
                );

                context.insert("emojis", &emoji);
                context.insert("stickers", &stickers);
                context.insert("streamer", &true);
                context.insert("stream_key", &found_user.stream_key);
            }
            return HttpResponse::Ok().body(templates::render("profile.html.tera", &context))
        }
    }
    return HttpResponse::SeeOther()
        .set_header(http::header::LOCATION, "/")
        .finish()
}
