use actix_http::http;
use actix_identity::*;
use actix_session::Session;
use actix_web::{web, post, HttpResponse};
use diesel::prelude::*;
use serde::Deserialize;
use uuid::Uuid;

use crate::ConnectionPool;
use crate::routes::util::*;
use crate::schema::users::dsl::*;
use crate::templates;
use crate::utils::session::*;
use crate::utils::user::*;

#[derive(Deserialize)]
pub struct NewUsernameForm<>{
    username: String,
}

#[post("/change_username")]
pub async fn change_username (
    form: web::Form<NewUsernameForm>,
    identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> HttpResponse {
    if let Some(iden) = identity.identity() {
        let connection = pool.get().expect("couldn't get db connection from pool");

        let newname: String = form.username
            .chars()
            .filter(|c| is_allowed_char(c) )
            .collect();

        let mut context = make_context(&session);
        context.insert("user", &iden);
        context.insert("page_title", &format!("profile for {}", &iden));

        match get_user_by_name(&iden, &connection) {
            Some(u) => {
                let now = chrono::Utc::now().naive_utc();

                if u.last_namechange + chrono::Duration::minutes(5) > now {
                    flash(
                        &session,
                        "error",
                        "you can only change your username once every five minutes",
                    );

                    return HttpResponse::SeeOther()
                        .body(templates::render("profile.html.tera", &context))

                } else {
                    match get_user_by_name(&newname, &connection) {
                        Some(_collision) => {
                            flash(
                                &session,
                                "error",
                                &format!("the name {} is already in use", &newname)
                            );

                            return HttpResponse::SeeOther()
                                .body(templates::render("profile.html.tera", &context))
                        },
                        None => {
                            diesel::update(users.filter(username.eq(&iden)))
                                .set((
                                    username.eq(&newname),
                                    last_namechange.eq(&now),
                                ))
                                .execute(&connection)
                                .unwrap();

                            identity.remember(String::from(&newname));

                            flash(
                                &session,
                                "info",
                                &format!("name updated successfully. hello, {}", &newname)
                            );

                            return HttpResponse::SeeOther()
                                .set_header(http::header::LOCATION, "/")
                                .finish()
                        },
                    }
                }
            },
            None => {
                flash(
                    &session,
                    "error",
                    &format!("failed to retrieve entry for '{}'; please contact an administrator", newname)
                );

                return HttpResponse::SeeOther()
                    .body(templates::render("profile.html.tera", &context))
            },
        }
    } else {
        return HttpResponse::SeeOther()
            .set_header(http::header::LOCATION, "/")
            .finish()
    }
}

#[post("/change_stream_key")]
pub async fn change_stream_key (
    identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> HttpResponse {
    as_streamer(identity, pool, |u, conn| {
        if u.live {
            flash(
                &session,
                "error",
                &format!("you can't change your key while you're streaming")
            );

            return HttpResponse::SeeOther()
                .set_header(http::header::LOCATION, "/profile")
                .finish()
        }

        let new_key = Uuid::new_v4().to_string();

        diesel::update(u)
            .set(stream_key.eq(&new_key))
            .execute(conn)
            .unwrap();

        HttpResponse::SeeOther()
            .set_header(http::header::LOCATION, "/profile")
            .finish()
    })
}
