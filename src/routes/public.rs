use actix_identity::*;
use actix_web::{web, get, HttpResponse, HttpRequest};
use actix_session::Session;

use crate::ConnectionPool;
use crate::stream_info::StreamInfo;
use crate::templates;
use crate::routes::util::*;

#[get("/")]
pub async fn index(
    _req: HttpRequest,
    _stream: web::Payload,
    id: Identity,
    pool: web::Data<ConnectionPool>,
    session: Session,
    stream_info: web::Data<StreamInfo>,
) -> HttpResponse {
    let connection = pool.get().expect("couldn't get db connection from pool");
    let context = everything_bagel(stream_info, id, &connection, session);
    HttpResponse::Ok().body(templates::render("combined.html.tera", &context))
}

#[get("/reset")]
pub async fn reset(
    _req: HttpRequest,
    session: Session,
) -> HttpResponse {
    let context = make_context(&session);
    HttpResponse::Ok().body(templates::render("reset.html.tera", &context))
}

#[get("/chat")]
pub async fn chat(
    _req: HttpRequest,
    _stream: web::Payload,
    id: Identity,
    pool: web::Data<ConnectionPool>,
    session: Session,
    stream_info: web::Data<StreamInfo>,
) -> HttpResponse {
    let connection = pool.get().expect("couldn't get db connection from pool");
    let mut context = everything_bagel(stream_info, id, &connection, session);
    context.insert("chatonly", &true);

    HttpResponse::Ok().body(templates::render("chat-only.html.tera", &context))
}

#[get("/video")]
pub async fn video(
    _req: HttpRequest,
    _id: Identity,
    _stream: web::Payload,
    session: Session,
) -> HttpResponse {
    let context = make_context(&session);
    HttpResponse::Ok().body(templates::render("video-only.html.tera", &context))
}
