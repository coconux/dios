use actix_web::{web, get, post, HttpRequest, HttpResponse, Error};
use actix_http::http;
use actix_identity::*;
use actix_session::Session;
use argon2::{self, Config};
use diesel::prelude::*;
use rand::prelude::*;
use serde::Deserialize;

use crate::ConnectionPool;
use crate::models::*;
use crate::routes::util::*;
use crate::schema::password_resets;
use crate::schema::users::dsl::*;
use crate::templates;
use crate::utils::mail::*;
use crate::utils::session::*;
use crate::utils::user::*;

#[derive(Deserialize)]
pub struct NewPasswordForm<>{
    reset_code: String,
    password: String,
    password_conf: String,
}

#[derive(Deserialize)]
pub struct ResetForm<>{
    email: String,
}

#[post("/reset")]
pub async fn reset(
    form: web::Form<ResetForm>,
    _identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> Result<HttpResponse, Error> {
    let connection = pool.get().expect("couldn't get db connection from pool");

    match get_user_by_email(&form.email, &connection) {
        Ok(Some(found_user)) => {
            let reset = create_reset_for(
                &found_user.email,
                &connection,
            );
            send_reset_email(reset);
            flash(
                &session,
                "info",
                "password reset email sent; valid for 1 hour"
            );
        },
        Ok(None) => {
            flash(
                &session,
                "error",
                "no user exists with that email address."
            );
        },
        Err(e) => panic!("Failed to query DB for email {}: {:?}", &form.email, e),
    }

    Ok(HttpResponse::SeeOther()
       .set_header(http::header::LOCATION, "/")
       .finish())
}

#[get("/new_password")]
pub async fn new_password(
    req: HttpRequest,
    _identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> Result<HttpResponse, Error> {
    let connection = pool.get().expect("couldn't get db connection from pool");

    let query: Vec<&str> = req.query_string().split('=').collect();
    let querycode = query.get(1);
    if querycode == None {
        return Ok(HttpResponse::SeeOther()
                  .set_header(http::header::LOCATION, "/")
                  .finish())
    }
    let reset_code = querycode.unwrap();

    let find_reset = password_resets::table
        .filter(password_resets::id.eq(reset_code))
        .first::<PasswordReset>(&connection)
        .optional()
        .unwrap();

    if let Some(found_reset) = find_reset {
        if found_reset.expires_at > chrono::Utc::now().naive_utc() {
            let mut context = make_context(&session);

            context.insert("reset_code", &found_reset.id);
            context.insert("email", &found_reset.email);
            context.insert("page_title", "reset password");

            return Ok(HttpResponse::Ok().body(templates::render("new_password.html.tera", &context)))
        } else {
            session.set("error", flash(
                &session,
                "error",
                "password reset code is expired"
            )).expect("failed to update session");
            return Ok(HttpResponse::SeeOther()
                      .set_header(http::header::LOCATION, "/reset")
                      .finish())
        }
    } else {
        session.set("error", flash(
            &session,
            "error",
            "invalid password reset code"
        )).expect("failed to update session");
        return Ok(HttpResponse::SeeOther()
                  .set_header(http::header::LOCATION, "/reset")
                  .finish())
    }
}

#[post("/new_password")]
pub async fn new_password_callback(
    form: web::Form<NewPasswordForm>,
    _identity: Identity,
    session: Session,
    pool: web::Data<ConnectionPool>,
) -> Result<HttpResponse, Error> {
    let pw = &form.password;
    let pw_chk = &form.password_conf;
    let connection = pool.get().expect("couldn't get db connection from pool");

    let reset_code = &form.reset_code;
    let find_reset = password_resets::table
        .filter(password_resets::id.eq(reset_code))
        .first::<PasswordReset>(&connection)
        .optional()
        .unwrap();

    if let Some(found_reset) = find_reset {
        let now = chrono::Utc::now().naive_utc();

        if found_reset.expires_at > now {
            if pw != pw_chk {
                session.set("error", flash(
                    &session,
                    "error",
                    "passwords do not match"
                )).expect("failed to update session");

                let mut context = make_context(&session);
                context.insert("reset_code", &found_reset.id);
                context.insert("email", &found_reset.email);
                return Ok(HttpResponse::Ok().body(templates::render("new_password.html.tera", &context)))
            } else {
                let salt: u64 = random();
                let config = Config::default();

                let hashed_pw = argon2::hash_encoded(&pw.as_bytes(), &salt.to_be_bytes(), &config).unwrap();
                diesel::update(users.filter(email.eq(&found_reset.email)))
                    .set(password_hash.eq(&hashed_pw))
                    .execute(&connection)
                    .unwrap();

                diesel::update(
                    password_resets::table
                        .filter(password_resets::id.eq(reset_code)))
                    .set(password_resets::expires_at.eq(now))
                    .execute(&connection)
                    .unwrap();

                flash(
                    &session,
                    "info",
                    "new password set"
                );

                return Ok(HttpResponse::SeeOther()
                          .set_header(http::header::LOCATION, "/")
                          .finish())
            }
        } else {
            flash(
                &session,
                "error",
                "password reset code is expired"
            );

            return Ok(HttpResponse::SeeOther()
                      .set_header(http::header::LOCATION, "/reset")
                      .finish())
        }
    } else {
        flash(
            &session,
            "error",
            "invalid password reset code"
        );

        return Ok(HttpResponse::SeeOther()
                  .set_header(http::header::LOCATION, "/reset")
                  .finish())
    }
}
