use actix_http::http;
use actix_identity::*;
use actix_session::Session;
use actix_web::{web, HttpResponse};
use diesel::prelude::*;
use std::env;
use tera::Context;

use crate::ConnectionPool;
use crate::models;
use crate::schema::server_about;
use crate::stream_info::StreamInfo;
use crate::utils::markdown;
use crate::utils::user::*;

fn get_page_title() -> String {
    let title_env = "APP_TITLE";
    let page_title = match env::var(title_env) {
        Ok(val) => val,
        Err(_) => String::from("DIOS"),
    };

    page_title
}

pub fn make_context(session: &Session) -> tera::Context {
    let mut context = Context::new();
    context = merge_session_into_context(&session, context);
    context.insert("page_title", &get_page_title());

    context
}

pub fn add_stream_info_to_context(
    stream_info: web::Data<StreamInfo>,
    context: tera::Context,
) -> tera::Context {
    let mut ctx = context.clone();

    let empty = String::from("");
    let title_op = stream_info.title.read().unwrap();
    let title = title_op.as_ref().unwrap_or(&empty);
    ctx.insert("title", &title);

    ctx
}

pub fn add_server_info_to_context(
    connection: &PgConnection,
    context: tera::Context,
) -> tera::Context {
    let mut ctx = context.clone();

    if let Ok(server_about) = server_about::table.find("1").first::<models::ServerAbout>(connection) {
        let rendered_blurb = match &server_about.blurb {
            Some(blub) => Some(markdown::parse_inline(&blub)),
            None => None,
        };

        ctx.insert("welcome", &server_about.welcome_message);
        ctx.insert("blurb", &rendered_blurb);
    } else {
        ctx.insert("welcome", "hello and welcome");
    }

    ctx
}

pub fn add_user_info_to_context(
    id: Identity,
    connection: &PgConnection,
    context: tera::Context,
) -> tera::Context {
    let mut ctx = context.clone();

    if let Some(id) = id.identity() {
        ctx.insert("user", &id);

        if let Some(found_user) = get_user_by_name(&id, connection) {
            if user_has_stream_powers(&found_user) {
                ctx.insert("user_is_streamer", &true);
                ctx.insert("stream_key", &found_user.stream_key);
                ctx.insert("app_domain", &env::var("APP_DOMAIN").expect("APP_DOMAIN is unset"));
            }
        }
    }

    if let Some(live_streamer) = who_is_live(connection) {
        ctx.insert("currently_streaming", &live_streamer.username);
    }

    ctx
}

pub fn everything_bagel(
    stream_info: web::Data<StreamInfo>,
    id: Identity,
    connection: &PgConnection,
    session: Session,
) -> tera::Context {
    let mut context = make_context(&session);
    context = add_stream_info_to_context(stream_info, context);
    context = add_server_info_to_context(&connection, context);
    context = add_user_info_to_context(id, &connection, context);

    context
}

pub fn merge_session_into_context(
    session: &Session,
    context: tera::Context,
) -> tera::Context {
    let mut ctx = context.clone();

    let alerts = vec![
        "info",
        "warning",
        "error",
    ];

    alerts.iter().for_each( |&key| ctx.insert(key, &get_vec_from_session(key, session)));

    ctx
}

fn get_vec_from_session(
    key: &str,
    session: &Session
) -> Vec<String> {
    let sesh = match session.get(key) {
        Ok(sesh) => {
            match sesh {
                Some(val) => val,
                None => Vec::new(),
            }
        },
        Err(_) => Vec::new()
    };

    session.remove(key);

    sesh
}

pub fn as_streamer<F>(
    identity: Identity,
    pool: web::Data<ConnectionPool>,
    block: F,
) -> HttpResponse
where F: Fn(&models::User, &PgConnection) -> HttpResponse {
    if let Some(iden) = identity.identity() {
        let connection = pool.get().expect("couldn't get db connection from pool");

        if let Some(u) = get_user_by_name(&iden, &connection) {
            if !user_has_stream_powers(&u) {
                return HttpResponse::Forbidden()
                    .set_header(http::header::LOCATION, "/")
                    .finish()
            }

            block(&u, &connection)
        } else {
            return HttpResponse::SeeOther()
                .set_header(http::header::LOCATION, "/")
                .finish()
        }
    } else {
        return HttpResponse::SeeOther()
            .set_header(http::header::LOCATION, "/")
            .finish()
    }
}
