table! {
    confirmations (id) {
        id -> Varchar,
        email -> Varchar,
        expires_at -> Timestamp,
    }
}

table! {
    password_resets (id) {
        id -> Varchar,
        email -> Varchar,
        expires_at -> Timestamp,
    }
}

table! {
    server_about (id) {
        id -> Varchar,
        welcome_message -> Nullable<Varchar>,
        blurb -> Nullable<Varchar>,
    }
}

table! {
    users (id) {
        id -> Varchar,
        username -> Varchar,
        password_hash -> Varchar,
        moderator -> Bool,
        streamer -> Bool,
        email -> Varchar,
        activated -> Bool,
        indefinitely_banned -> Bool,
        last_namechange -> Timestamp,
        stream_key -> Nullable<Varchar>,
        live -> Bool,
    }
}

allow_tables_to_appear_in_same_query!(
    confirmations,
    password_resets,
    server_about,
    users,
);
