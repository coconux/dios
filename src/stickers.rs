
use std::env;
use std::io::{Error, ErrorKind};
use std::path::{Path, PathBuf};
use std::fs;
// use std::vec;
use std::iter::Iterator;
use std::collections::HashMap;
use std::cmp;
use serde::{Serialize, Deserialize};

use crate::emojo;
use crate::inlines::{CustomInline,Sticker};


/// Structure for stickers.json
#[derive(Serialize, Deserialize, PartialEq, Eq, Ord)]
pub struct SerializedSticker {
    pub name: String,
    pub url: String
}


impl PartialOrd for SerializedSticker  {
    fn partial_cmp(&self, rhs: &Self) -> Option<cmp::Ordering>  {
        self.name.partial_cmp(&rhs.name)
    }
}

///Returns the paths of all stickers in a sticker pack's directory
pub fn stickers_from_path(path: &PathBuf) -> Vec<Option<String>> {
    match fs::read_dir(path) {
            Ok(dir) => dir.map(|e| sticker_from_path(&e.unwrap().path())).collect(),
            Err(_) => vec!(None)
        }
}

///Returns the paths of all stickers in the stickers/ directory
pub fn sticker_paths(path: &Path) -> Vec<Option<String>> {
    let mut stickers = Vec::new();

    let dir_contents = match fs::read_dir(path) {
        Ok(contents) => contents,
        Err(e) => panic!(
            "Bad path {}: {}",
            path.to_str().unwrap(),
            e
        )
    };

    for entry in dir_contents {
        let entry_path = entry.unwrap().path().join("png");
        let serve_path = entry_path.strip_prefix("./data").unwrap();
        stickers.extend(stickers_from_path(&entry_path)
                        .iter()
                        .map(|s| match s {
                            Some(p) => Some(format!("{}/{}", &serve_path.to_str().unwrap(), p)),
                            None => None})
                        .collect::<Vec<Option<String>>>());
    }

    stickers
}

///Create a list of sticker packs with URLs so we can render the sticker interface for admins
pub fn stickers_for_display(path: &Path) -> Vec<Sticker> {
    let mut stickers = Vec::new();

    let dir_contents = match fs::read_dir(path) {
        Ok(contents) => contents,
        Err(e) => panic!(
            "Bad path {}: {}",
            path.to_str().unwrap(),
            e
        )
    };

    for entry in dir_contents {
        let entry_path = entry.unwrap().path();
        let pack = entry_path.file_name().unwrap().to_str().to_owned().unwrap();
        stickers.extend(stickers_from_path(&entry_path.join("png"))
                        .iter()
                        .filter(|opt| opt.is_some())
                        .map(|sticker| sticker.clone().unwrap())
                        .map(|sticker| Sticker {pack: pack.to_owned(), name: sticker})
                        .collect::<Vec<Sticker>>());
    }

    stickers
}

///Create a hashmap of packs and stickers, suitable for serialization to JSON with Serde
pub fn stickers_for_serialize(path: &Path) -> HashMap<String, Vec<SerializedSticker>> {
    let mut stickers = HashMap::<String, Vec<SerializedSticker>>::new();

    let dir_contents = match fs::read_dir(path) {
        Ok(contents) => contents,
        Err(e) => panic!(
            "Bad path {}: {}",
            path.to_str().unwrap(),
            e
        )
    };

    let filtered_dir_contents = dir_contents
            .filter(|e| e.is_ok()) //Drop errors
            .map(|e| e.unwrap()) //Unwrapping's okay
            .filter(|e| e.file_type().unwrap().is_dir()); //Drop files like stickers.json

    for maybe_entry in filtered_dir_contents {
        let entry = maybe_entry;
        let key =  entry.file_name().to_str().unwrap().to_string();
        let sticker_vector = serialized_stickers_from_path(&key, &entry.path().join("png"));
        stickers.insert(key, sticker_vector);
    }

    stickers
}

///Generate the fillename from a sticker's path
pub fn sticker_from_path(path: &Path) -> Option<String> {
    if path.is_dir() {
        return None;
    }

    if path.extension().is_none() {
        return None;
    }

    if path.extension().unwrap() != "png" {
        return None;
    }

    let filename = path
        .file_name()
        .unwrap()
        .to_str()
        .unwrap();

    Some(str::replace(filename, ".png", ""))
}

///Get a pack's stickers in a format for serializing
pub fn serialized_stickers_from_path(pack: &str, base: &Path) -> Vec<SerializedSticker> {
    if !base.is_dir() {
        return Vec::new(); //for now
    }
    let maybe_stickers = stickers_from_path(&base.to_path_buf());

    let mut sorted_stickers: Vec<SerializedSticker> = maybe_stickers.iter()
        .filter(|opt| opt.is_some())
        .map(|sticker| sticker.clone().unwrap())
        .map(|sticker| SerializedSticker {url: format!{"stickers/{}/png/{}.png", pack, &sticker}, name: sticker})
            .collect();

    sorted_stickers.sort();
    sorted_stickers
}

//FS Functions

pub fn rename_sticker(
    old_name: &CustomInline,
    new_name: &Sticker
    ) -> Result<(), std::io::Error> {
    let data_root = match env::var("DATA_ROOT") {
        Ok(val) => val,
        Err(_) => String::from("/data"),
    };

    let cleaned_newname = emojo::make_allowed_shortcode(&new_name.name);

    let old = match old_name  {
        CustomInline::Emojo(e) => format!("{}/emoji/png/{}.png", &data_root, e),
        CustomInline::Sticker(s) =>  format!("{}/stickers/{}/png/{}.png", &data_root, s.pack, s.name) };
    let new = &format!("{}/stickers/{}/png/{}.png", &data_root, new_name.pack, cleaned_newname);

    let old_path = Path::new(&old);
    let new_path = Path::new(new);

    if !old_path.is_file() {
        warn!("{:?} is not file", &old_path);
        return Err(Error::new(ErrorKind::InvalidInput, "not a file"))
    }
    if new_path.exists() {
        warn!("{:?} already exists", &new_path);
        return Err(Error::new(ErrorKind::AlreadyExists, "won't overwrite existing file"))
    }

    match fs::create_dir_all(new_path.parent().unwrap()) {
        Ok(_) => {},
        Err(e) => return Err(e),
    };

    match fs::rename(old_path, new_path) {
        Ok(_) => Ok(()),
        Err(e) => {
            error!("cant move {:?}", e);
            Err(e)
        },
    }

}

pub fn delete_sticker(name: &str) -> Result<(), Error>  {
    let data_root = match env::var("DATA_ROOT") {
        Ok(val) => val,
        Err(_) => String::from("/data"),
    };

    let sticker = Sticker::from_string(&name).expect("Not a sticker.");

    let path = &format!("{}/stickers/{}/png/{}.png", &data_root, sticker.pack, sticker.name);
    let emojo = Path::new(path);

    if !emojo.is_file() {
        warn!("{:?} is not file", &emojo);
        return Err(Error::new(ErrorKind::NotFound, format!("{:?} is not file", &emojo)));
    }
    fs::remove_file(emojo) 
}

// SERIALIZE

pub fn make_json_from_stickers(path: &Path) -> String {

    let sticker_map = stickers_for_serialize(path);

    serde_json::to_string(&sticker_map).unwrap()
}

///Regreate the stickers.json file
pub fn update_sticker_list(png_dir: &str) -> Result<(), Error> {

    let source_dir = Path::new(png_dir);
    if !source_dir.is_dir() {
        return Err(Error::new(ErrorKind::Other,"Not a Directory" ))
    }
    let json = make_json_from_stickers(source_dir);

    let data_root = match env::var("DATA_ROOT") {
        Ok(val) => val,
        Err(_) => String::from("/data"),
    };
    let custom_json = format!("{}/stickers/stickers.json", data_root);
    match fs::write(&custom_json, json) {
        Ok(_) => {
            debug!("successfully wrote {}", &custom_json);
            Ok(())
        },
        Err(e) => {
            error!("Failed to write {}: {:?}", &custom_json, e);
            Err(e)
        }
    }
}
