use comrak::nodes::{AstNode};
use comrak::{ComrakOptions, Arena, parse_document, format_html};

fn default_options() -> ComrakOptions {
    let mut options = ComrakOptions::default();
    options.render.escape = true;
    options.extension.strikethrough = true;
    options.extension.autolink = true;

    options
}

pub fn parse_inline (message: &str) -> String {
    let options = default_options();
    // allocate memory pool
    //
    // I believe this is used because parent nodes reference their children and
    // vice-versa.
    let arena = Arena::new();

    let node = parse_document(&arena, message, &options);
    render_node(node, &options)
}

fn render_node<'a> (node: &'a AstNode<'a>, options: &ComrakOptions) -> String {
    let mut inline_contents = String::new();

    for child in node.children() {
        let val = &child.data.borrow().value;
        let is_block = val.block();

        match (val, is_block) {
            (comrak::nodes::NodeValue::Image(contents), _) => {
                inline_contents += &format!(
                    "<a href={0}>{0}<a/>",
                    String::from_utf8(contents.url.clone()).unwrap()
                );
            },
            (_, false) |
            // because we're escaping HTML, HtmlBlocks become inline (but still say
            // they're blocks):
            (comrak::nodes::NodeValue::HtmlBlock(_), _)   => {
                let mut buffer = vec![];
                format_html(child, options, &mut buffer).unwrap();
                inline_contents += &String::from_utf8(buffer).expect("Conversion failed!");
            },
            _ => {
                inline_contents += &render_node(child, options);
            }
        }
    }
    String::from(inline_contents)
}

#[cfg(test)]
#[path="../../tests/utils/markdown_test.rs"]
mod markdown_test;
