use actix_session::Session;

pub fn flash(
    session: &Session,
    key: &str,
    val: &str,
) -> () {
    let mut alerts: Vec<String> = match session.get(key) {
        Ok(sesh) => {
            match sesh {
                Some(value) => value,
                None => Vec::new(),
            }
        },
        Err(_) => Vec::new(),
    };

    alerts.push(String::from(val));

    match session.set(key, alerts) {
        Ok(_) => (),
        Err(e) => error!("failed setting session for flash message: {:?}", e),
    }
}
