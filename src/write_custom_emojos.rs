#[macro_use] extern crate log;
use dotenv::dotenv;
use std::env;

pub mod emojo;
pub mod inlines;
use crate::emojo::*;

fn main () {
    dotenv().ok();

    let argv: Vec<String> = env::args().collect();

    let png_dir = match argv.get(1) {
        Some(dir) => dir,
        None => panic!("write_custom_emojos requires a directory argument")
    };

    update_emojo_list(&png_dir);
    ()
}
