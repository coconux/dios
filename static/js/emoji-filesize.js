(function(){
  const submitButton = document.querySelector('#emoji-upload');
  if (!submitButton) return;
  submitButton.disabled = true;

  const maxSize = document.querySelector('#emoji-max-size');

  function validateSize(e) {
    if (e.target.files[0].size < 250000) {
      submitButton.disabled = false;
      maxSize.classList.remove('error');
    } else {
      submitButton.disabled = true;
      maxSize.classList.add('error');
    }
  }

  const fileInput = document.querySelector('#emoji-file-input');
  if (!fileInput) return;

  fileInput.addEventListener('change', validateSize, false);
})();
