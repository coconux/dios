import { EmojiButton } from '@joeattardi/emoji-button'

(async function() {
  const customJSON = await fetch('/emoji/custom.json')
  const customs = await customJSON.json()

  const picker = new EmojiButton({ custom: customs, 
    initialCategory: 'recents'
  })
  const button = document.querySelector('#emoji-trigger')
  const chatbox = document.querySelector('#text')

  picker.on('emoji', selection => {
    let insert

    if (selection.custom) {
      insert = `:${selection.name}:`
    } else {
      insert = selection.emoji
    }

    chatbox.value += insert
  })

  picker.on('hidden', () => {
    chatbox.focus()
  })

  button.addEventListener('click', () => picker.togglePicker(button))
})()
