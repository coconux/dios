(function(){
  const submitButton = document.querySelector('#poster-upload');
  if (!submitButton) return;
  submitButton.disabled = true;

  const maxSize = document.querySelector('#poster-max-size');

  function validateSize(e) {
    if (e.target.files[0].size < 1000000) {
      submitButton.disabled = false;
      maxSize.classList.remove('error');
    } else {
      submitButton.disabled = true;
      maxSize.classList.add('error');
    }
  }

  const fileInput = document.querySelector('#poster-file-input');
  if (!fileInput) return;

  fileInput.addEventListener('change', validateSize, false);
})();
