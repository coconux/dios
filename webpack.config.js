const path = require('path');

module.exports = {
  mode: 'production',
  entry: {
    chat: [
      path.resolve(__dirname, 'static', 'js', 'chat.js'),
      path.resolve(__dirname, 'static', 'js', 'emoji-picker.js'),
    ],
    profile: [
      path.resolve(__dirname, 'static', 'js', 'emoji-filesize.js'),
      path.resolve(__dirname, 'static', 'js', 'poster-filesize.js')
    ],
    video: path.resolve(__dirname, 'static', 'js', 'video.js'),
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'static', 'dist'),
  },
};
